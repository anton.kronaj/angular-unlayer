import {Component, OnInit, ViewChild} from '@angular/core';
import {EmailEditorComponent} from 'angular-email-editor';

// import MergeTags from '../assets/MergeTags-200.json';
// import MergeTags from '../assets/MergeTags-500.json';
import MergeTags from '../assets/MergeTags-1067.json';
// import MergeTags from '../assets/MergeTags-2140.json';
// import MergeTags from '../assets/MergeTags-4759.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angular-unlayer';
  public options: object = {};

  @ViewChild(EmailEditorComponent)
  private emailEditor: EmailEditorComponent | undefined;


  constructor() {
    console.log('Total Merge Tags: ', MergeTags.length);
  }

  ngOnInit() {
    this.options = {
      features: {
        smartMergeTags: false,
      },
    };
  }

// called when the editor is created
  editorLoaded(event: any) {
    console.log('editorLoaded');
    // load the design json here
    // this.emailEditor.editor.loadDesign({});
    // @ts-ignore
    this.emailEditor.editor.setMergeTags(MergeTags);
  }

  // called when the editor has finished loading
  editorReady(event: any) {
    console.log('editorReady');
  }

  exportHtml() {
    // @ts-ignore
    this.emailEditor.editor.exportHtml((data: any) =>
      console.log('exportHtml', data)
    );
  }

}
