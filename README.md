# AngularUnlayer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.6.

Its purpose is to demonstrate Unlayer editor handling of large number of merge tags. Edit `app.component.ts file` to see how different number of merge tags affect the user experience. Import the different `MergeTags-200.json` files.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.


